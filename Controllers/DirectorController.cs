﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using LPE_Magac.Models;
using LPE_Magac.ViewModel;
using System.Data.Entity.Validation;
using System.Data.Entity;

namespace LPE_Magac.Controllers
{
    [AllowAnonymous]
    public class DirectorController : Controller
    {

        Db_LPE_MagacEntities db = new Db_LPE_MagacEntities();
        // GET: Director
        public ActionResult Index()
        {
            var IOtable = new FundsModel
            {
                Incomes = db.tbl_fund_income.ToList(),
                Outcomes = db.tr_fund_outcome.ToList(),
                Requests = db.tr_fund_request.ToList()
            };
            return View(IOtable);
        }
        public ActionResult GetDataIncome()
        {
            var query = db.tbl_fund_income.GroupBy(p => p.date_income.Value.Year)
                .Select(g => new { date = g.Key, count = g.Sum(x => x.total_fund_income) }).OrderBy(x => x.date).ToList();
            return Json(query, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDataOutcome()
        {
            var query = db.tr_fund_outcome.GroupBy(p => p.date_outcome.Value.Year)
                .Select(g => new { date = g.Key, count = g.Sum(x => x.total_fund_outcome) }).OrderBy(x => x.date).ToList();
            return Json(query, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FundTable()
        {
            var IOtable = new FundsModel
            {
                Incomes = db.tbl_fund_income.ToList(),
                Outcomes = db.tr_fund_outcome.ToList()
            };
            return View(IOtable);
        }
        //=========Req Funds===============
        public ActionResult RequestFunds()
        {
            List<tbl_invoice_director> directors = db.tbl_invoice_director.ToList();
            List<tbl_invoice_finance> invoice = db.tbl_invoice_finance.ToList();
            List<tr_fund_request> request = db.tr_fund_request.ToList();
            var ListRequest = from a in directors
                              join b in invoice on a.fk_id_invoice_finance equals b.pk_id_invoice_finance
                              join c in request on b.fk_id_fund_request equals c.pk_id_fund_request into table
                              from c in table.ToList()
                              select new InvoiceModel
                              {
                                  Invoice_Director = a,
                                  Invoice_Finance = b,
                                  Fund_Request = c
                              };
            return View(ListRequest);
        }
        [HttpGet]
        public ActionResult ApproveListRequest(int id)
        {
            var finance = db.tbl_invoice_finance.Where(x => x.pk_id_invoice_finance == id).FirstOrDefault();
            var id1 = finance.fk_id_fund_request;
            InvoiceModel invoice = new InvoiceModel();
            invoice.Fund_Request = (from b in db.tr_fund_request
                                    where b.pk_id_fund_request == id1
                                    select b).SingleOrDefault();
            invoice.Invoice_Finance = (from b in db.tbl_invoice_finance
                                       where b.pk_id_invoice_finance == id
                                       select b).SingleOrDefault();
            invoice.Invoice_Director = (from b in db.tbl_invoice_director
                                        where b.fk_id_invoice_finance == id
                                        select b).SingleOrDefault();
            return PartialView(invoice);
        }
        [HttpPost]
        public ActionResult ApproveListRequest(int id, InvoiceModel invoice)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var updatedir = db.tbl_invoice_director.Where(x => x.fk_id_invoice_finance == id).FirstOrDefault();
                    updatedir.status_invoice_director = "Approved";
                    updatedir.details = invoice.Invoice_Director.details;
                    db.Entry(updatedir).State = EntityState.Modified;
                    var selectid = db.tbl_invoice_finance.Where(x => x.pk_id_invoice_finance == id).FirstOrDefault();
                    selectid.status_invoice_finance = "App Dir";
                    db.Entry(selectid).State = EntityState.Modified;
                    var idreq = selectid.fk_id_fund_request;
                    var updatereq = db.tr_fund_request.Where(x => x.pk_id_fund_request == idreq).FirstOrDefault();
                    updatereq.status_request = "Approved";
                    db.Entry(updatereq).State = EntityState.Modified;
                    tr_fund_outcome input = new tr_fund_outcome()
                    {
                        service_outcome = updatereq.service_request,
                        date_outcome = updatereq.date_request,
                        applicant_outcome = updatereq.applicant_request,
                        price_per_unit_request = updatereq.price_per_unit_request,
                        quantity_request = updatereq.quantity_request,
                        total_fund_outcome = updatereq.total_value,
                        fk_id_invoice_director = updatedir.pk_id_invoice_director,
                        details_outcome = updatereq.details_request
                    };
                    db.tr_fund_outcome.Add(input);
                    db.SaveChanges();
                    var select = db.tr_fund_outcome.Where(x => x.fk_id_invoice_director == updatedir.pk_id_invoice_director).FirstOrDefault();
                    tbl_finance_report report = new tbl_finance_report()
                    {
                        fk_id_fund_outcome = select.pk_id_fund_outcome,
                        //balance = -(input.total_fund_outcome)
                    };
                    db.tbl_finance_report.Add(report);
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("RequestFunds");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : ApproveListRequest, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(invoice);
        }
        [HttpGet]
        public ActionResult RejectListRequest(int id)
        {
            var finance = db.tbl_invoice_finance.Where(x => x.pk_id_invoice_finance == id).FirstOrDefault();
            var id1 = finance.fk_id_fund_request;
            InvoiceModel invoice = new InvoiceModel();
            invoice.Fund_Request = (from b in db.tr_fund_request
                                    where b.pk_id_fund_request == id1
                                    select b).SingleOrDefault();
            invoice.Invoice_Finance = (from b in db.tbl_invoice_finance
                                       where b.pk_id_invoice_finance == id
                                       select b).SingleOrDefault();
            invoice.Invoice_Director = (from b in db.tbl_invoice_director
                                        where b.fk_id_invoice_finance == id
                                        select b).SingleOrDefault();
            return PartialView(invoice);
        }
        [HttpPost]
        public ActionResult RejectListRequest(int id, InvoiceModel invoice)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var selectid = db.tbl_invoice_finance.Where(x => x.pk_id_invoice_finance == id).FirstOrDefault();
                    selectid.status_invoice_finance = "Block";
                    db.Entry(selectid).State = EntityState.Modified;
                    var idreq = selectid.fk_id_fund_request;
                    var updatereq = db.tr_fund_request.Where(x => x.pk_id_fund_request == idreq).FirstOrDefault();
                    updatereq.status_request = "Block";
                    db.Entry(updatereq).State = EntityState.Modified;
                    var selectdir = db.tbl_invoice_director.Where(x => x.fk_id_invoice_finance == id).FirstOrDefault();
                    selectdir.status_invoice_director = "Block";
                    selectdir.details = invoice.Invoice_Director.details;
                    db.Entry(selectdir).State = EntityState.Modified;
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("RequestFunds");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : RejectListRequest, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(invoice);
        }

        //=============================
        public ActionResult Guide()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        [HttpGet]
        public ActionResult FormOutcome()
        {
            return View();
        }
        [HttpPost]
        public ActionResult FormOutcome(tr_fund_request request)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_invoice_finance invoice = new tbl_invoice_finance();
                    invoice.fk_id_fund_request = request.pk_id_fund_request;
                    invoice.status_invoice_finance = "Send Dir";
                    db.tbl_invoice_finance.Add(invoice);
                    //===========
                    tbl_invoice_director invoiceDir = new tbl_invoice_director();
                    invoiceDir.fk_id_invoice_finance = invoice.pk_id_invoice_finance;
                    invoiceDir.status_invoice_director = "App Fin";
                    db.tbl_invoice_director.Add(invoiceDir);
                    //===========
                    request.status_request = "Send Dir";
                    request.date_request = DateTime.Now;
                    db.tr_fund_request.Add(request);
                    //===========
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("RequestFunds");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : FormOutcome, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(request);
        }
        public ActionResult ViewIncome()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                //====Table Size======
                SizeToReportContent = true,
                AsyncRendering = true
            };

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("datein", "1/1/1901");
            //reportParameter[1] = new ReportParameter("dateout", "1/1/1901");

            report.ServerReport.ReportPath = "/Income";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();

            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ViewOutcome()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                //====Table Size======
                SizeToReportContent = true,
                AsyncRendering = true
            };

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("datein", "1/1/1901");
            //reportParameter[1] = new ReportParameter("dateout", "1/1/1901");

            report.ServerReport.ReportPath = "/Outcome";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();

            ViewBag.ReportViewer = report;
            return View();
        }

    }
}