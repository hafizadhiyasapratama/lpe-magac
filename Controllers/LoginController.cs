﻿using LPE_Magac.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace LPE_Magac.Controllers
{
    public class LoginController : Controller
    {
        Db_LPE_MagacEntities db = new Db_LPE_MagacEntities();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Autherize(tbl_user user)
        {
            {
                var userDetails = db.tbl_user.Where(x => x.user_email == user.user_email && x.user_password == user.user_password).FirstOrDefault();
                if (userDetails != null)
                {
                    var role = userDetails.user_position;

                    if (role == "director")
                    {
                        Session["userID"] = userDetails.pk_id_user;
                        Session["userName"] = userDetails.user_name;
                        Session["userPosition"] = "director";
                        Session["userEmail"] = userDetails.user_email;
                        Session["userPassword"] = userDetails.user_password;
                        Session["userAddress"] = userDetails.user_address;
                        Session["userPicture"] = userDetails.user_profile_picture;
                        return RedirectToAction("Index", "Director");
                    }
                    else if (role == "technician")
                    {
                        Session["userID"] = userDetails.pk_id_user;
                        Session["userName"] = userDetails.user_name;
                        Session["userPosition"] = "technician";
                        Session["userEmail"] = userDetails.user_email;
                        Session["userPassword"] = userDetails.user_password;
                        Session["userAddress"] = userDetails.user_address;
                        Session["userPicture"] = userDetails.user_profile_picture;
                        return RedirectToAction("Index", "Technician");
                    }
                    else
                    {
                        Session["userID"] = userDetails.pk_id_user;
                        Session["userName"] = userDetails.user_name;
                        Session["userPosition"] = "finance";
                        Session["userEmail"] = userDetails.user_email;
                        Session["userPassword"] = userDetails.user_password;
                        Session["userAddress"] = userDetails.user_address;
                        Session["userPicture"] = userDetails.user_profile_picture;
                        return RedirectToAction("Index", "Finance");
                    }
                }
                else
                {
                    user.LoginErrorMessage = "Wrong username or password.";
                    return View("Index", user);
                }
            }
        }
        public ActionResult Logout()
        {
            int userId = (int)Session["userID"];
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }

        public ActionResult Register()
        {
            return View();
        }
        public JsonResult CheckUsernameAvailability(string userdata)
        {
            try
            {
                System.Threading.Thread.Sleep(200);
                var SeachData = db.tbl_register.Where(x => x.register_email == userdata).SingleOrDefault();
                if (SeachData != null)
                {
                    return Json(1);
                }
                else
                {
                    return Json(0);
                }
            }
            catch (Exception)
            {
                return Json("index", userdata);
            }

        }
        [HttpPost]
        public ActionResult SaveRegister(HttpPostedFileBase file, tbl_register register)
        {
            try
            {
                tbl_register inputdata = new tbl_register();
                if (file != null && file.ContentLength > 0)
                {
                    string imgname = Path.GetFileName(file.FileName);
                    string ext = Path.GetExtension(imgname);
                    if (ext == ".jpg" || ext == ".png" || ext == ".jpeg" || ext == ".JPG" || ext == ".PNG" || ext == ".JPEG")
                    {
                        string imgpatch = Path.Combine(Server.MapPath("~/Content/images/avatar"), imgname);
                        file.SaveAs(imgpatch);
                        inputdata.register_profile_picture = imgname;
                    }
                }
                var SeachData = db.tbl_register.Where(x => x.register_email == register.register_email).SingleOrDefault();
                if (SeachData == null)
                {
                    inputdata.register_name = register.register_name;
                    inputdata.pk_id_register = register.pk_id_register;
                    inputdata.register_password = register.register_password;
                    inputdata.register_email = register.register_email;
                    inputdata.register_position = register.register_position;
                    inputdata.register_address = register.register_address;
                    db.tbl_register.Add(inputdata);
                    db.SaveChanges();
                    return View("Index");
                }
                else
                {
                    register.RegisterErrorMessage = "That email is taken.Try Another.";
                    return View("Register", register);
                }
            }
            catch
            {
                return View("Register",register);
            }
            
        }
    }
}