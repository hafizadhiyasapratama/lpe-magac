﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using LPE_Magac.Models;
using LPE_Magac.ViewModel;
using System.Data.Entity.Validation;
using System.IO;
using System.Data.Entity;

namespace LPE_Magac.Controllers
{
    [AllowAnonymous]
    public class TechnicianController : Controller
    {
        Db_LPE_MagacEntities db = new Db_LPE_MagacEntities();
        // GET: Technician
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult MyProfile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MyProfile(tbl_temp_user user)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.tbl_temp_user.Add(user);
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("index");
                }
                catch (Exception)
                {
                    Transaction.Rollback();
                }
            }
            return View(user);
        }
        //============Req Funds==============
        [HttpGet]
        public ActionResult RequestFunds()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RequestFunds(tr_fund_request request)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_invoice_finance invoice = new tbl_invoice_finance();
                    invoice.fk_id_fund_request = request.pk_id_fund_request;
                    invoice.status_invoice_finance = "Request";
                    db.tbl_invoice_finance.Add(invoice);
                    //===========
                    tbl_invoice_director invoiceDir = new tbl_invoice_director();
                    invoiceDir.fk_id_invoice_finance = invoice.pk_id_invoice_finance;
                    invoiceDir.status_invoice_director = "At Fin";
                    db.tbl_invoice_director.Add(invoiceDir);
                    //===========
                    request.status_request = "Send Fin";
                    request.date_request = DateTime.Now;
                    db.tr_fund_request.Add(request);
                    //===========
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("ListRequestFunds");
                }
                catch (Exception)
                {
                    Transaction.Rollback();
                }
            }
            return View(request);
        }

        public ActionResult ListRequestFunds()
        {
            List<tbl_invoice_director> directors = db.tbl_invoice_director.ToList();
            List<tbl_invoice_finance> invoice = db.tbl_invoice_finance.ToList();
            List<tr_fund_request> request = db.tr_fund_request.ToList();
            var ListRequest = from a in directors
                              join b in invoice on a.fk_id_invoice_finance equals b.pk_id_invoice_finance
                              join c in request on b.fk_id_fund_request equals c.pk_id_fund_request into table
                              from c in table.ToList()
                              orderby c.date_request descending
                              select new InvoiceModel
                              {
                                  Invoice_Director = a,
                                  Invoice_Finance = b,
                                  Fund_Request = c
                              };
            return View(ListRequest);
        }
        public ActionResult Guide()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        [HttpGet]
        public ActionResult SaveEdit()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveEdit(HttpPostedFileBase file, tbl_temp_user edited)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_temp_user inputdata = new tbl_temp_user();
                    if (file != null && file.ContentLength > 0)
                    {
                        string imgname = Path.GetFileName(file.FileName);
                        string ext = Path.GetExtension(imgname);
                        if (ext == ".jpg" || ext == ".png" || ext == ".jpeg" || ext == ".JPG" || ext == ".PNG" || ext == ".JPEG")
                        {
                            string imgpatch = Path.Combine(Server.MapPath("~/Content/images/avatar"), imgname);
                            file.SaveAs(imgpatch);
                            inputdata.temp_user_profile_picture = imgname;
                        }
                    }
                    inputdata.pk_id_temp_user = edited.pk_id_temp_user;
                    inputdata.temp_user_name = edited.temp_user_name;
                    inputdata.temp_user_password = edited.temp_user_password;
                    inputdata.temp_user_email = edited.temp_user_email;
                    inputdata.temp_user_position = edited.temp_user_position;
                    inputdata.temp_user_address = edited.temp_user_address;
                    inputdata.fk_id_user = edited.fk_id_user;
                    db.tbl_temp_user.Add(inputdata);
                    db.SaveChanges();
                    Transaction.Commit();
                    return View("Index");
                }
                catch (Exception)
                {
                    Transaction.Rollback();
                    edited.ErrorMessage = "Required field cannot be empty.";
                }
            }
            return View("MyProfile", edited);
        }
    }
}