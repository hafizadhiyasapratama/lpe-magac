﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using LPE_Magac.Models;
using LPE_Magac.ViewModel;
using System.Data.Entity.Validation;
using System.Data.Entity;

namespace LPE_Magac.Controllers
{
    [AllowAnonymous]
    public class FinanceController : Controller
    {
        Db_LPE_MagacEntities db = new Db_LPE_MagacEntities();
        // GET: Finance
        public ActionResult Index()
        {
            var IOtable = new FundsModel
            {
                Incomes = db.tbl_fund_income.ToList(),
                Outcomes = db.tr_fund_outcome.ToList(),
                Requests = db.tr_fund_request.ToList()
            };
            return View(IOtable);
        }
        public ActionResult GetDataIncome()
        {
            var query = db.tbl_fund_income.GroupBy(p => p.date_income.Value.Year)
                .Select(g => new { date = g.Key, count = g.Sum(x => x.total_fund_income) }).OrderBy(x => x.date).ToList();
            return Json(query, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDataOutcome()
        {
            var query = db.tr_fund_outcome.GroupBy(p => p.date_outcome.Value.Year)
                .Select(g => new { date = g.Key, count = g.Sum(x => x.total_fund_outcome) }).OrderBy(x => x.date).ToList();
            return Json(query, JsonRequestBehavior.AllowGet);
        }
        //====================================================================
        //Fund Table
        public ActionResult FundTable()
        {
            var IOtable = new FundsModel
            {
                Incomes = db.tbl_fund_income.ToList(),
                Outcomes = db.tr_fund_outcome.ToList()
            };
            return View(IOtable);     
        }
        //====================================================================
        public ActionResult IncomeEdit(int id)
        {
            return View(db.tbl_fund_income.Where(x => x.pk_id_fund_income == id).FirstOrDefault());
        }
        public ActionResult IncomeDelete(int id)
        {
            return View(db.tbl_fund_income.Where(x => x.pk_id_fund_income == id).FirstOrDefault());
        }
        //==========================
        [HttpPost]
        public ActionResult IncomeEdit(int id, tbl_fund_income income)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(income).State = EntityState.Modified;
                    tbl_finance_report EditIncomeReport = db.tbl_finance_report.Where(x => x.fk_id_fund_income == id).FirstOrDefault();
                    EditIncomeReport.date_report = income.date_income;
                    db.Entry(EditIncomeReport).State = EntityState.Modified;
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("FundTable");
                }
                catch(Exception e) {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : IncomeEdit, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(income);
        }
        [HttpPost]
        public ActionResult IncomeDelete(int id, tbl_fund_income income)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var removeeReport = db.tbl_finance_report.Where(x => x.fk_id_fund_income == id).FirstOrDefault();
                    db.tbl_finance_report.Remove(removeeReport);
                    var removeeIncome = db.tbl_fund_income.Where(x => x.pk_id_fund_income == id).FirstOrDefault();
                    db.tbl_fund_income.Remove(removeeIncome);
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("FundTable");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : IncomeDelete, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(income);
        }
        [HttpGet]
        public ActionResult FormIncome()
        {
            return View();
        }
        [HttpPost]
        public ActionResult FormIncome(tbl_fund_income income)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.tbl_fund_income.Add(income);
                    tbl_finance_report inputdata = new tbl_finance_report
                    {
                        fk_id_fund_income = income.pk_id_fund_income,
                        date_report = income.date_income
                    };
                    db.tbl_finance_report.Add(inputdata);
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("FundTable");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : FormIncome, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(income);
        }
        //====================================================================
        //public ActionResult OutcomeEdit(int id)
        //{
        //    return View(db.tr_fund_outcome.Where(x => x.pk_id_fund_outcome == id).FirstOrDefault());
        //}
        //public ActionResult OutcomeDelete(int id)
        //{
        //    return View(db.tr_fund_outcome.Where(x => x.pk_id_fund_outcome == id).FirstOrDefault());
        //}
        //==========================
        //[HttpPost]
        //public ActionResult OutcomeEdit(int id, tr_fund_outcome outcome)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        tbl_finance_report EditOutcomeReport = db.tbl_finance_report.Where(x => x.fk_id_fund_income == id).FirstOrDefault();
        //        EditOutcomeReport.date_report = outcome.date_outcome;
        //        db.Entry(outcome).State = EntityState.Modified;
        //        //EditOutcomeReport.balance = +(outcome.total_fund_outcome);
        //        db.Entry(EditOutcomeReport).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("FundTable");
        //    }
        //    return View(outcome);
        //}
        //[HttpPost]
        //public ActionResult OutcomeDelete(int id, tr_fund_outcome outcome)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var removeeOutcome = db.tr_fund_outcome.Where(x => x.pk_id_fund_outcome == id).FirstOrDefault();
        //        var removeeReport = db.tbl_finance_report.Where(x => x.fk_id_fund_income == id).FirstOrDefault();
        //        db.tbl_finance_report.Remove(removeeReport);
        //        db.tr_fund_outcome.Remove(removeeOutcome);
        //        db.SaveChanges();
        //        return RedirectToAction("FundTable");
        //     }
        //    return View(outcome);
        //}

        [HttpGet]
        public ActionResult FormOutcome()
        {
            return View();
        }
        [HttpPost]
        public ActionResult FormOutcome(tr_fund_request request)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_invoice_finance invoice = new tbl_invoice_finance
                    {
                        fk_id_fund_request = request.pk_id_fund_request,
                        status_invoice_finance = "Send Dir"
                    };
                    db.tbl_invoice_finance.Add(invoice);
                    //===========
                    tbl_invoice_director invoiceDir = new tbl_invoice_director
                    {
                        fk_id_invoice_finance = invoice.pk_id_invoice_finance,
                        status_invoice_director = "App Fin"
                    };
                    db.tbl_invoice_director.Add(invoiceDir);
                    //===========
                    request.status_request = "Send Dir";
                    request.date_request = DateTime.Now;
                    db.tr_fund_request.Add(request);
                    //===========
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("ListRequest");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : FormOutcome, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(request);
        }
        //====================================================================
        //====================================================================
        //====================================================================
        //List Request
        public ActionResult ListRequest()
        {
            List<tbl_invoice_finance> invoice = db.tbl_invoice_finance.ToList();
            List<tr_fund_request> request = db.tr_fund_request.ToList();
            var ListRequest = from a in invoice
                              join b in request on a.fk_id_fund_request equals b.pk_id_fund_request into table
                              from b in table.ToList()
                              orderby b.date_request descending
                              select new InvoiceModel
                              {
                                  Invoice_Finance = a,
                                  Fund_Request = b
                              };
            return View(ListRequest);
        }
        [HttpGet]
        public ActionResult ApproveListRequest(int id)
        {
            InvoiceModel invoice = new InvoiceModel
            {
                Fund_Request = (from b in db.tr_fund_request
                                where
                                    b.pk_id_fund_request == id
                                select b).SingleOrDefault(),
                Invoice_Finance = (from b in db.tbl_invoice_finance
                                   where b.fk_id_fund_request == id
                                   select b).SingleOrDefault()
            };
            return PartialView(invoice);
        }
        [HttpPost]
        public ActionResult ApproveListRequest(int id, InvoiceModel invoice)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var edit = db.tbl_invoice_finance.Where(x => x.fk_id_fund_request == id).FirstOrDefault();
                    edit.status_invoice_finance = "Send Dir";
                    edit.details = invoice.Invoice_Finance.details;
                    db.Entry(edit).State = EntityState.Modified;
                    var updatereq = db.tr_fund_request.Where(x => x.pk_id_fund_request == id).FirstOrDefault();
                    updatereq.status_request = "Send Dir";
                    db.Entry(edit).State = EntityState.Modified;
                    var updatedir = db.tbl_invoice_director.Where(x => x.fk_id_invoice_finance == edit.pk_id_invoice_finance).FirstOrDefault();
                    updatedir.status_invoice_director = "App Fin";
                    db.Entry(updatedir).State = EntityState.Modified;
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("ListRequest");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : ApproveListRequest, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(invoice);
        }
        [HttpGet]
        public ActionResult RejectListRequest(int id)
        {
            InvoiceModel invoice = new InvoiceModel
            {
                Fund_Request = (from b in db.tr_fund_request
                                where b.pk_id_fund_request == id
                                select b).SingleOrDefault(),
                Invoice_Finance = (from b in db.tbl_invoice_finance
                                   where b.fk_id_fund_request == id
                                   select b).SingleOrDefault()
            };
            return PartialView(invoice);
        }
        [HttpPost]
        public ActionResult RejectListRequest(int id, InvoiceModel invoice)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                { 
                    var edit = db.tbl_invoice_finance.Where(x => x.fk_id_fund_request == id).FirstOrDefault();
                    edit.status_invoice_finance = "Block";
                    edit.details = invoice.Invoice_Finance.details;
                    db.Entry(edit).State = EntityState.Modified;
                    var change = db.tr_fund_request.Where(x => x.pk_id_fund_request == id).FirstOrDefault();
                    change.status_request = "Block";
                    db.Entry(change).State = EntityState.Modified;
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("ListRequest");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : RejectListRequest, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(invoice);
        }

        //====Report Funds=====
        public ActionResult ReportFunds()
        {
            List<tbl_fund_income> income = db.tbl_fund_income.ToList();
            List<tr_fund_outcome> outcome = db.tr_fund_outcome.ToList();
            List<tbl_finance_report> report = db.tbl_finance_report.ToList();
            var FinanceReport = from a in report
                              join b in income on a.fk_id_fund_income equals b.pk_id_fund_income
                              join c in outcome on a.fk_id_fund_outcome equals c.pk_id_fund_outcome into table
                              from c in table.ToList()
                              select new JoinModel
                              {
                                  report = a,
                                  income = b,
                                  outcome = c
                              };
            return View(FinanceReport);
        }
        public new ActionResult User()
        {
            var UserTable = new ListUser
            {
                Registers = db.tbl_register.Where(x => x.status == null).ToList(),
                Temp_Users = db.tbl_temp_user.ToList(),
                Users = db.tbl_user.ToList()
            };
            return View(UserTable);
        }
        //=====Accepting User=====
        [HttpGet]
        public ActionResult AccReqisterUser(int id)
        {
            var detailsregister = db.tbl_register.Where(x => x.pk_id_register == id).FirstOrDefault();
            return View(detailsregister);
        }
        [HttpPost]
        public ActionResult AccReqisterUser(int id, tbl_register register)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var detailsregister = db.tbl_register.Where(x => x.pk_id_register == id).FirstOrDefault();
                    tbl_register reg = new tbl_register()
                    {
                        register_name = detailsregister.register_name,
                        register_email = detailsregister.register_email,
                        register_password = detailsregister.register_password,
                        register_position = detailsregister.register_position,
                        register_address = detailsregister.register_address,
                        register_profile_picture = detailsregister.register_profile_picture,
                        status = "accepted"
                    };
                    db.tbl_register.Add(reg);
                    db.tbl_register.Remove(detailsregister);
                    tbl_user user = new tbl_user()
                    {
                        fk_id_register = reg.pk_id_register,
                        user_name = reg.register_name,
                        user_email = reg.register_email,
                        user_password = reg.register_password,
                        user_position = reg.register_position,
                        user_address = reg.register_address,
                        user_profile_picture = reg.register_profile_picture,
                    };
                    db.tbl_user.Add(user);
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("User");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : AccRegisterUser, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(register);
        }
        //=====Blocking User=====
        [HttpGet]
        public ActionResult BlockRegister(int id)
        {
            var detailsregister = db.tbl_register.Where(x => x.pk_id_register == id).FirstOrDefault();
            return View(detailsregister);
        }
        [HttpPost]
        public ActionResult BlockRegister(int id, tbl_register register)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var detailsregister = db.tbl_register.Where(x => x.pk_id_register == id).FirstOrDefault();
                    db.tbl_register.Remove(detailsregister);
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("User");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : BlockRegister, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(register);
        }
        //=====Delete User=====
        [HttpGet]
        public ActionResult DeleteUser(int id)
        {
            var details = db.tbl_user.Where(x => x.pk_id_user == id).FirstOrDefault();
            return View(details);
        }
        public ActionResult DeleteUser(int id, tbl_register register)
        {
            var details = db.tbl_user.Where(x => x.pk_id_user == id).FirstOrDefault();
            var detailsregister = db.tbl_register.Where(x => x.pk_id_register == details.fk_id_register).FirstOrDefault();
            db.tbl_user.Remove(details);
            db.tbl_register.Remove(detailsregister);
            db.SaveChanges();
            return RedirectToAction("User");
        }
        //=====Accepting Edit User=====
        [HttpGet]
        public ActionResult AccEditUser(int id)
        {
            var detailsEdit = db.tbl_temp_user.Where(x => x.pk_id_temp_user == id).FirstOrDefault();
            return View(detailsEdit);
        }
        [HttpPost]
        public ActionResult AccEditUser(int id, tbl_temp_user temp)
        {
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var detailsEdit = db.tbl_temp_user.Where(x => x.pk_id_temp_user == id).FirstOrDefault();
                    tbl_temp_user Edited = new tbl_temp_user()
                    {
                        temp_user_name = detailsEdit.temp_user_name,
                        temp_user_email = detailsEdit.temp_user_email,
                        temp_user_password = detailsEdit.temp_user_password,
                        temp_user_position = detailsEdit.temp_user_position,
                        temp_user_address = detailsEdit.temp_user_address,
                        temp_user_profile_picture = detailsEdit.temp_user_profile_picture,
                        fk_id_user = detailsEdit.fk_id_user,
                        status = "accepted"
                    };
                    db.tbl_temp_user.Add(Edited);
                    var detailsUser = db.tbl_user.Where(x => x.pk_id_user == Edited.fk_id_user).FirstOrDefault();

                    detailsUser.user_name = detailsEdit.temp_user_name;
                    detailsUser.user_email = detailsEdit.temp_user_email;
                    detailsUser.user_password = detailsEdit.temp_user_password;
                    detailsUser.user_position = detailsEdit.temp_user_position;
                    detailsUser.user_address = detailsEdit.temp_user_address;
                    detailsUser.user_profile_picture = detailsEdit.temp_user_profile_picture;

                    db.Entry(detailsUser).State = EntityState.Modified;
                    db.tbl_temp_user.Remove(detailsEdit);
                    db.tbl_temp_user.Remove(Edited);
                    db.SaveChanges();
                    Transaction.Commit();
                    return RedirectToAction("User");
                }
                catch (Exception e)
                {
                    Db_LPE_MagacEntities db1 = new Db_LPE_MagacEntities();
                    tbl_log_history error = new tbl_log_history()
                    {
                        user_name = (string)Session["userName"],
                        fk_id_user = (int)Session["userID"],
                        Action_date = DateTime.Now,
                        ErrorMessage = "Page Error : AccEditUser, " + e.Message
                    };
                    db1.tbl_log_history.Add(error);
                    db1.SaveChanges();
                    Transaction.Rollback();
                }
            }
            return View(temp);
        }
        //=====Reject Edit User=====
        [HttpGet]
        public ActionResult RejectEditUser(int id)
        {
            var details = db.tbl_temp_user.Where(x => x.pk_id_temp_user == id).FirstOrDefault();
            return View(details);
        }
        public ActionResult RejectEditUser(int id, tbl_temp_user tempUser)
        {
            var details = db.tbl_temp_user.Where(x => x.pk_id_temp_user == id).FirstOrDefault();
            db.tbl_temp_user.Remove(details);
            db.SaveChanges();
            return RedirectToAction("User");
        }

        //=====Log History=====
        public ActionResult LogHistory()
        {
            var error = new ListError
            {
                Errors = db.tbl_log_history.ToList()
            };
            return View(error);
        }
        //=====Guide=====
        public ActionResult Guide()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        //=====Report Builder : =====
        public ActionResult FundReport()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                //====Table Size======
                //report.Width = Unit.Pixel(1100);
                SizeToReportContent = true,
                AsyncRendering = true
            };

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("datein", "1/1/1901");
            report.ServerReport.ReportPath = "/FinanceReport";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ViewKwitansi(int id)
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                //====Table Size======
                SizeToReportContent = true,
                AsyncRendering = true
            };

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("inputID", id.ToString());
            report.ServerReport.ReportPath = "/Kwitansi";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ViewInvoice(int id)
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                //====Table Size======
                SizeToReportContent = true,
                AsyncRendering = true
            };

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("inputID", id.ToString());
            report.ServerReport.ReportPath = "/Invoice";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ViewIncome()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                //====Table Size======
                SizeToReportContent = true,
                AsyncRendering = true
            };

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("datein", "1/1/1901");
            report.ServerReport.ReportPath = "/Income";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
        public ActionResult ViewOutcome()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                //====Table Size======
                SizeToReportContent = true,
                AsyncRendering = true
            };

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //=====parameters=====
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("datein", "1/1/1901");
            report.ServerReport.ReportPath = "/Outcome";
            //=====parameters=====
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}