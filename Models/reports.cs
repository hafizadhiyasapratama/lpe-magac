﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPE_Magac.Models
{
    public class Reports
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public List<tbl_fund_income> INCOMES { get; set; }
        public List<tr_fund_outcome> OUTCOMES { get; set; }
    }
}