﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace LPE_Magac.Models
{
    [MetadataType(typeof(tr_fund_request_MetaData))]
    public partial class tr_fund_request
    {

    }
    public partial class tr_fund_request_MetaData
    {
        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "This field is required.")]
        public Nullable<double> price_per_unit_request { get; set; }
        [DataType(DataType.Currency)]
        public Nullable<double> total_value { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string service_request { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public Nullable<System.DateTime> date_request { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public Nullable<int> quantity_request { get; set; }
    }
}