﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LPE_Magac.Models
{
    [MetadataType(typeof(tbl_register_MetaData))]
    public partial class tbl_register
    {
        public string RegisterErrorMessage { get; set; }
    }
    public class tbl_register_MetaData
    {
        [DataType(DataType.Upload)]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string register_profile_picture { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string register_name { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string register_email { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string register_password { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string register_address { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string register_position { get; set; }
    }
}