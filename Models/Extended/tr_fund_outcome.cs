﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace LPE_Magac.Models
{
    [MetadataType(typeof(tr_fund_outcome_MetaData))]
    public partial class tr_fund_outcome
    {
    }
    public partial class tr_fund_outcome_MetaData
    {
        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "This field is required.")]
        public Nullable<double> price_per_unit_request { get; set; }

        [DataType(DataType.Currency)]
        public Nullable<double> total_fund_outcome { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public Nullable<System.DateTime> date_outcome { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string service_outcome { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public Nullable<double> quantity_request { get; set; }
    }

}