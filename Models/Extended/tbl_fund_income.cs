﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace LPE_Magac.Models
{
   [MetadataType(typeof(tbl_fund_income_Metadata))]
    public partial class tbl_fund_income
    {
    }
    public class tbl_fund_income_Metadata
    {
        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "This field is required.")]
        public Nullable<int> total_fund_income { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public Nullable<System.DateTime> date_income { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public string service_income { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public string applicant_income { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string address_income { get; set; }
    }
}