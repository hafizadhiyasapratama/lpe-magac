﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LPE_Magac.Models
{
    [MetadataType(typeof(tbl_temp_user_MetaData))]
    public partial class tbl_temp_user
    {
        public string ErrorMessage { get; set; }
    }
    public class tbl_temp_user_MetaData
    {
        [Required(ErrorMessage = "This field is required.")]
        public string temp_user_name { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string temp_user_email { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "This field is required.")]
        public string temp_user_password { get; set; }
        public string temp_user_address { get; set; }
        [DataType(DataType.Upload)]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string temp_user_profile_picture { get; set; }
    }
}