﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LPE_Magac.Models
{
    [MetadataType(typeof(tbl_user_MetaData))]
    public partial class tbl_user
    {
        public string LoginErrorMessage { get; set; }
    }
    public class tbl_user_MetaData
    {
        [Required(ErrorMessage = "This field is required.")]
        public string user_email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "This field is required.")]
        public string user_password { get; set; }
        [DataType(DataType.Upload)]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string user_profile_picture { get; set; }
    }
}