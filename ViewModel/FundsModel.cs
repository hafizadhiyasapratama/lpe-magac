﻿using LPE_Magac.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPE_Magac.ViewModel
{
    public class FundsModel
    {
        public IEnumerable<tr_fund_outcome> Outcomes { get; set; }
        public IEnumerable<tbl_fund_income> Incomes { get; set; }
        public IEnumerable<tbl_finance_report> Reports { get; set; }
        public IEnumerable<tr_fund_request> Requests { get; set; }
    }
    public class ListUser
    {
        public IEnumerable<tbl_register> Registers { get; set; }
        public IEnumerable<tbl_user> Users { get; set; }
        public IEnumerable<tbl_temp_user> Temp_Users { get; set; }
    }
    public class JoinModel
    {
        public tbl_fund_income income { get; set; }
        public tr_fund_outcome outcome { get; set; }
        public tbl_finance_report report { get; set; }
    }
    public class InvoiceModel
    {
        public tbl_invoice_finance Invoice_Finance { get; set; }
        public tbl_invoice_director Invoice_Director { get; set; }
        public tr_fund_request Fund_Request { get; set; }
    }
    public class ListError
    {
        public IEnumerable<tbl_log_history> Errors { get; set; }
    }
}