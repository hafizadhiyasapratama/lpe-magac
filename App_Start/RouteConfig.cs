﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LPE_Magac
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Finance",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Finance", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Director",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Director", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Technician",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Technician", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
