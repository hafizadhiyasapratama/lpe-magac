create database Db_LPE_Magac

use Db_LPE_Magac

create table tbl_register (
pk_id_register int identity primary key,
register_name varchar(50),
register_email varchar(50),
register_password varchar(10),
register_address	varchar(255),
register_position varchar(50),
register_profile_picture varchar(255),
status varchar(50))

create table tbl_user (
pk_id_user int identity primary key,
user_name varchar(50),
user_email varchar(50),
user_password varchar(10),
user_address	varchar(255),
user_position varchar(50),
user_profile_picture varchar(255),
fk_id_register int foreign key references tbl_register(pk_id_register),
status varchar(50))

create table tbl_temp_user (
pk_id_temp_user int identity primary key,
temp_user_name varchar(50),
temp_user_email varchar(50),
temp_user_password varchar(10),
temp_user_address	varchar(255),
temp_user_position varchar(50),
temp_user_profile_picture varchar(255),
fk_id_user int foreign key references tbl_user(pk_id_user),
status varchar(50))

create table tbl_log_history
(pk_id_record int identity primary key,
user_name varchar(50),
fk_id_user int foreign key references tbl_user(pk_id_user),
Action_date		date,
ErrorMessage 	varchar(255))

create table tbl_fund_income
(pk_id_fund_income int identity primary key,
date_income date,
service_income	varchar(50),
applicant_income varchar(50),
total_fund_income float,
details_income varchar(255))

create table tr_fund_request
(pk_id_fund_request int identity primary key,
service_request varchar(100),
date_request			date,
quantity_request		int,
price_per_unit_request	float,
total_value 			float,
details_request varchar(100),
status_request varchar(50),
fk_id_user int foreign key references tbl_user(pk_id_user))

create table tbl_invoice_finance
(pk_id_invoice_finance int identity primary key,
status_invoice_finance varchar(10),
details varchar(255),
fk_id_fund_request int foreign key references tr_fund_request(pk_id_fund_request))

create table tbl_invoice_director
(pk_id_invoice_director int identity primary key,
status_invoice_director varchar(10),
details varchar(255),
fk_id_invoice_finance int foreign key references tbl_invoice_finance(pk_id_invoice_finance))

create table tr_fund_outcome
(pk_id_fund_outcome int identity primary key,
date_outcome date,
applicant_outcome varchar(50),
service_outcome	varchar(50),
quantity_request float,
price_per_unit_request	float,
total_fund_outcome 		float,
details_outcome 		varchar(255),
fk_id_invoice_director int foreign key references tbl_invoice_director(pk_id_invoice_director))

create table tbl_finance_report
(pk_id_finance_report int identity primary key,
fk_id_fund_income int foreign key references tbl_fund_income(pk_id_fund_income),
fk_id_fund_outcome int foreign key references tr_fund_outcome(pk_id_fund_outcome))


//masukan data berikut :

select *from tbl_register
select *from tbl_user

insert into tbl_register values ('rian','rian@gmail.com','rian','Bogor','finance',''),
('subyantoro','subyantoro@gmail.com','subyantoro','Jakarta','director',''),
('joko','joko@gmail.com','joko','Tanggerang','technician','')

insert into tbl_user values ('rian','rian@gmail.com','rian','Bogor','finance','','1'),
('subyantoro','subyantoro@gmail.com','subyantoro','Jakarta','director','','2'),
('joko','joko@gmail.com','joko','Tanggerang','technician','','3')